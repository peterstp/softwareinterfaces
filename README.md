# README #

## What is this repository for? ##

This repository contains some basic tools for communicating between different programming languages using file-based interfaces. Currently the repository just contains code for calling R from MATLAB.

## How do I get set up? ##

### Calling R from MATLAB

* Install git if necessary (https://www.atlassian.com/git/tutorials/install-git)

* Open a Terminal/Command Prompt window, navigate to where you want to store a local copy of the repository, and clone the repository from Git:

```
cd /Users/peter/Documents
git clone https://pmcharrison@bitbucket.org/pmcharrison/softwareinterfaces.git
```

* Launch R and install the R package:

```
library(devtools)
install_bitbucket("pmcharrison/SoftwareInterfaces/InterfaceR")
```

* Add the `InterfaceMATLAB` directory to your MATLAB path

* Find the line in `call_R.m` that defines the variable `rscript_path`, and change the path to the path to Rscript on your local computer. Save the file. 

* Call R from within MATLAB:

```
call_r('a <- rnorm(10)', 'a')
```