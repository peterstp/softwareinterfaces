# Code for interfacing with MATLAB

#' Write an R object to a MATLAB file.
#'
#' Writes an R object to a text file that can be read and evaluated by MATLAB.
#' @param x R object to write.
#' @param file Text file to write to.
#' @param overwrite Whether or not to overwrite an existing text file at that location.
#' @export
write_object_to_file_matlab <- function(x, file, overwrite = TRUE) {
  if (file.exists(file)) {
    if (overwrite) {
      file_removed <- file.remove(file)
      if (!file_removed) stop(sprintf("File (%s) could not be removed.", file))
    } else {
      stop(sprintf("File (%s) existed already, and was not permitted to be overwritten (try setting overwrite = TRUE).", file))
    }
  }
  write(format_matlab(x), file, append = FALSE)
}

#' Format an R object as a MATLAB string.
#'
#' Formats an R object as a MATLAB string.
#' @param x Object to be formatted.
#' @return Object formatted as a MATLAB string.
#' @export
format_matlab <- function(x) {
  if (is.matrix(x)) stop("Methods do not yet exist for matrices.")
  if (is.array(x)) stop("Methods do not yet exist for arrays")
  UseMethod("format_matlab", x)
}

format_matlab.numeric <- function(x) {
  sprintf("[%s]", paste(x, collapse = "; "))
}

format_matlab.character <- function(x) {
  x %>% paste("'", ., "'", sep = "") %>%
    paste(., collapse = "; ") %>%
    paste("string({", ., "})", sep = "")
}

format_matlab.logical <- function(x) {
  x %>% as.character %>% tolower %>%
    paste(., collapse = "; ") %>%
    paste("[", ., "]", sep = "")
}

format_matlab.list <- function(x) {
    vapply(x, format_matlab, character(1)) %>%
    paste(., collapse = "; ") %>%
    paste("{", ., "}", sep = "")
}
