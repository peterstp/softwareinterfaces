function out = import_object_from_file(file)
fid = fopen(file);
res = textscan(fid, '%s', 'delimiter','\n');
res = res{1};
res = strjoin(res, '');
out = eval(res);
end