function out = call_r(command, outputs, rscript_path)
% Runs an R command and returns the output to MATLAB.
% The R command should set the output(s) as global variables;
% these global variables are then retrieved by name and returned
% to MATLAB either as a single object (if only one output was 
% specified) or as a cell array (if multiple outputs were specified).
% <command>: string containing the command to run in R.
% <outputs>: either a string identifying the R object to return
% to MATLAB, or a string vector identifying multiple R objects 
% to return to MATLAB.
% <rscript_path> should be the path to the Rscript.exe 
% R executable, with any space-containing components
% the file name enclosed in parentheses.
% Example:
% rscript_path = 'C:/"Program Files"/R/R-3.4.0/bin/Rscript.exe';
if isstring(outputs)
    single_output = false;
else
    single_output = true;
    outputs = string({outputs});
end
output_tmp_path = strrep([tempname, '.txt'], '\', '/');
export_cmd_1 = ' library("InterfaceR")';
export_cmd_2 = sprintf(' write_object_to_file_matlab(list(%s), "%s", overwrite = T)', ...
    strjoin(outputs, ', '), output_tmp_path);
new_command = strcat(command, ' ; ', export_cmd_1, ' ; ', export_cmd_2);
run_r_command(new_command, rscript_path);
out = import_object_from_file(output_tmp_path);
if single_output
    out = out{1};
end 
end

function out = run_r_command(command, rscript_path)
tmp_path = [tempname, '.R'];
fid = fopen(tmp_path, 'wt');
fprintf(fid, command);
fclose(fid);
out = run_r_script(tmp_path, rscript_path);
end

function out = run_r_script(script_path, rscript_path)
% Runs an R script located at file name <script_path>.
% <rscript_path> should be the path to the Rscript.exe 
% R executable, with any space-containing components
% the file name enclosed in parentheses.
% Example:
% rscript_path = 'C:/"Program Files"/R/R-3.4.0/bin/Rscript.exe';
out = system(sprintf('%s %s', rscript_path, script_path));
end

% Eventually we should code up a way of transferring MATLAB objects to 
% R objects. This could be done using the R.matlab package, though it is a
% bit slow.
% function out = create_r_object(name, matlab_obj)
% etc.